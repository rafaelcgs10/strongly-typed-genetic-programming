#set terminal png
set terminal pdf
set output 'erro.pdf'

set xlabel "geração"
#set autoscale

set ylabel "desvio padrão"

set style line 1 lc rgb "red" pt 7 ps 0.65 lw 1 dashtype 1 pi 25
set style line 2 lc rgb "orange" pt 8 ps 0.65 lw 1 dashtype 1 pi 25
set style line 3 lc rgb "green" pt 9 ps 0.65 lw 1 dashtype 1 pi 25
set style line 4 lc rgb "blue" pt 4 ps 0.65 lw 1 dashtype 1 pi 25
set style line 5 lc rgb "violet" pt 5 ps 0.65 lw 1 dashtype 1 pi 25
set style line 6 lc rgb '#000000' pt 6 ps 0.65 lw 1 dashtype 1 pi 25

set key outside
set key right top
set grid
set xrange [1:200]
set style function linespoints

plot 'fits_res_250.dat' using 1:3 with linespoints ls 1 title 'cs250', \
     'fits_res_500.dat' using 1:3 with linespoints ls 2  title 'cs500', \
     'fits_res_1000.dat' using 1:3 with linespoints ls 3  title 'cs1000', \
     'fits_250.dat' using 1:3 with linespoints ls 4  title 'nocs250', \
     'fits_500.dat' using 1:3 with linespoints ls 5  title 'nocs500', \
     'fits_1000.dat' using 1:3 with linespoints ls 6  title 'nocs1000'
