module Infer where

import Parser
import Control.Monad.Except
import Control.Monad.State as S
import qualified Data.Map as Map

data Type = TVar TVar | TArrow Type Type | TList Type deriving (Show, Eq)
rightType (TArrow _ r) = r
leftType (TArrow l _) = l

type TVar = String

data TErro = TUnificationStop Type Type | TInfinity TVar Type deriving (Show)
data Count = Count {value :: Int} deriving (Show)

type TSubst = Map.Map TVar Type
type Context = Map.Map Id Type
emptyContext = Map.empty :: Context

type Infer = ExceptT TErro (S.State Count)

-- Substituição de Tipo

tsubst :: Type -> TSubst -> Type
tsubst (TArrow l r) s = TArrow (tsubst l s) (tsubst r s)
tsubst (TVar tv) s = case Map.lookup tv s of
                          (Just tv') -> tv'
                          Nothing -> (TVar tv)

compose :: TSubst -> TSubst -> TSubst
s1 `compose` s2 = Map.map ((flip tsubst) s1) s2 `Map.union` s1

-- Unificação de tipos

unify :: Type -> Type -> Infer TSubst
unify (TArrow l r) (TArrow l' r') = do
      s1 <- unify l l'
      s2 <- unify (tsubst r s1) (tsubst r' s1)
      return $ s1 `compose` s2

unify (TVar a) t = bind a t
unify t (TVar a) = bind a t
unify t1 t2 = throwError $ TUnificationStop t1 t2

bind :: TVar -> Type -> Infer TSubst
bind a t
        | TVar a == t = return Map.empty
        | occurs a t = throwError $ TInfinity a t
        | otherwise = return $ Map.singleton a t

occurs :: TVar -> Type -> Bool
occurs tv (TVar t) = if tv == t then True else False
occurs tv (TArrow l r) = (occurs tv l) || (occurs tv r)

unifyE :: [Type] -> [Type] -> Infer TSubst
unifyE (x:xs) (y:ys) = unify (foldl TArrow x xs) (foldl TArrow y ys)

-- Gera nomes de variáveis

wordsN n = take n $ repeat ['a'..'z']
lexico = sequence . wordsN
lexicoInf = lex [1..] where lex (x:xs) =  lexico x ++ lex xs

newTVar :: Infer Type 
newTVar = do 
        c <- get
        let newName = lexicoInf !! (value c)
        put (Count ((value c) + 1))
        return (TVar newName)

-- Inferencia

lookContext :: Id -> Context -> Infer (Type, Context)
lookContext e c = case Map.lookup e c of
                  Just t  -> return (t, c)
                  Nothing -> do {nt <- newTVar; return (nt, Map.insert e nt c)}

infer :: Expr -> Context -> Infer (Type, Context)
infer e c = case e of
            Primi e' -> do 
                    nt1 <- newTVar
                    nt2 <- newTVar
                    return (TArrow (TArrow nt1 nt2) (TArrow (TList nt1) (TList nt2)), c)


            Var e' -> do 
                    (te, ce') <- lookContext e' c
                    return (te, ce')

            (Abs x e') -> do
                    (te', c') <- infer e' c
                    (tx, c'') <- lookContext x c'
                    let c''' = Map.delete x c''
                    let ta = TArrow tx te'
                    return (ta, c''')

            (App e' e'') -> do
                (te', c') <- infer e' c
                (te'', c'') <- infer e'' c
                case te' of
                     TArrow r _ -> do
                            ntv <- newTVar
                            let lc' = (Map.elems (Map.intersection c' c'')) ++ [leftType te', ntv]
                            let lc'' = (Map.elems (Map.intersection c'' c')) ++ [te'', ntv]
                            s <- unifyE lc' lc''
                            let ste' = tsubst te' s
                            let rste' = rightType ste'
                            let cu = Map.union c' c''
                            let c''' = Map.map (flip tsubst s) cu
                            return (rste', c''')
                     TVar _ -> do
                            ntv <- newTVar
                            ntv' <- newTVar
                            let lc' = (Map.elems (Map.intersection c' c'')) ++ [te', ntv]
                            let lc'' = (Map.elems (Map.intersection c'' c')) ++ [TArrow te'' ntv', ntv]
                            s <- unifyE lc' lc''
                            let sntv' = tsubst ntv' s
                            let cu = Map.union c' c''
                            let c''' = Map.map (flip tsubst s) cu
                            return (sntv', c''')

runInfer ast = runState (runExceptT (infer ast emptyContext)) (Count 0)

printType :: Type -> String
printType (TArrow l r) = "(" ++ (printType l) ++ " -> " ++ (printType r) ++ ")"
printType (TVar tv) = tv
printType (TList t) = "[" ++ (printType t) ++ "]"
printContext :: Context -> String
printContext c = printContext' $ Map.toList c
printContext' [] = ""
printContext' [(v, t)] = v ++ ":" ++ printType t
printContext' ((v, t):xs) = v ++ ":" ++ printType t ++ ", " ++ printContext' xs
