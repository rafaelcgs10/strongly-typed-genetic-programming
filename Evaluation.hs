 module Evaluation where

import Debug.Trace
import Parser
import Infer
import Data.Set (Set)
import qualified Data.Set as Set

data Subs = Subs Id Expr

freeVar :: Expr -> Set Id
freeVar (Var id) = Set.fromList [id]
freeVar (App e e') = Set.union (freeVar e) (freeVar e')
freeVar (Abs id e) = Set.difference (freeVar e) (Set.fromList [id])

varSubst :: Subs -> Expr -> Expr
varSubst (Subs id e) a@(Var id') = if id == id' then e else a
varSubst s (App e e') = App (varSubst s e) (varSubst s e')
varSubst s@(Subs id e) a@(Abs id' e') = if id == id' then a else 
                                                          if not (Set.member id (freeVar e')) then a else
                                                                                                  if not (Set.member id' (freeVar e)) then Abs id' (varSubst s e') else
                                                                                                    Abs (id'++"'") (varSubst s (varSubst (Subs id' (Var (id'++"'"))) e'))
                                                                                                     
bReduction :: Expr -> Expr
bReduction e@(App (Abs id m) (n)) = trace ("BREDU" ++ show e ++ " -> " ++ show (varSubst (Subs id n) m)) $  varSubst (Subs id n) m

isRedex :: Expr -> Bool
isRedex (App (Abs id m) (n)) = True
isRedex _ = False

eval :: Expr -> Expr
eval e@(App m n) = eval (bReduction (App m (eval n)))
eval e = e

safeEval :: Expr -> Expr
safeEval e = let (res, c) = runInfer e in case res of (Left _) -> e
                                                      (Right _) -> eval e
