import Infer
import Parser

main = do
    s <- getLine
    let ast = runParser s
    case ast of
        Right ast' -> case runInfer ast' of
            (Right (t, c), _) -> putStrLn $ printContext c ++ " |- " ++ s ++ ":" ++ printType t
            (Left t, _) -> putStrLn $ show t
        Left erro -> putStrLn $ show erro 
