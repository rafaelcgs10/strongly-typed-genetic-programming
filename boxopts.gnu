set terminal pdf
set output 'boxopts.pdf'

set style fill solid 0.15 border -1
set ylabel "n. avaliações de fitness"
set style boxplot outliers pointtype 1
set style data boxplot
set xtics ('nocs250' 1, 'nocs500' 2, 'nocs1000' 3, 'cs250' 4, 'cs500' 5, 'cs1000' 6)

plot 'opts_res_250b.dat' using (4):1 notitle, \
     'opts_res_500b.dat' using (5):1 notitle, \
     'opts_res_1000b.dat' using (6):1 notitle, \
     'opts_250b.dat' using (1):1 notitle, \
     'opts_500b.dat' using (2):1 notitle, \
     'opts_1000b.dat' using (3):1 notitle

