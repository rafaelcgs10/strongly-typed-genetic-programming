set terminal pdf
set output 'box.pdf'

set style fill solid 0.15 border -1
set ylabel "fitness"
set style boxplot outliers pointtype 1
set style data boxplot
set xtics ('nocs250' 1, 'nocs500' 2, 'nocs1000' 3, 'cs250' 4, 'cs500' 5, 'cs1000' 6)

plot 'last_res_250.dat' using (4):1 notitle, \
     'last_res_500.dat' using (5):1 notitle, \
     'last_res_1000.dat' using (6):1 notitle, \
     'last_250.dat' using (1):1 notitle, \
     'last_500.dat' using (2):1 notitle, \
     'last_1000.dat' using (3):1 notitle
