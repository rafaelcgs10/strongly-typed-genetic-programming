module Parser where

import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

type Id = String
data Expr = App Expr Expr | Var Id | Abs Id Expr | Primi Id |  List [Expr] | ARG deriving (Show)

-- Especificacao sintatica

abst = do
      reservedOp "\\"
      v <- var
      reservedOp "->"
      e <- expr
      return $ Abs v  e

list = do
      reservedOp "["
      l <- sepBy expr comma
      reservedOp "]"
      return $ List l

primiCAR = do
     reservedOp "MAPCAR"
     let v = "MAPCAR"
     return $ Primi v

primicons = do
     reservedOp "cons"
     let v = "cons"
     return $ Primi v

primicdr = do
     reservedOp "cdr"
     let v = "cdr"
     return $ Primi v

appO = [[Infix (return (App)) AssocLeft]]

expr :: Parser Expr
expr = buildExpressionParser appO expr'

expr' = parens expr <|> abst <|> list <|> primiCAR <|> primicons <|> primicdr <|> liftM Var var

comma = do
        reservedOp ","
        return ()

-- Especificacao lexica

languageDef = emptyDef { 
          Token.identStart      = letter
         , Token.identLetter     = alphaNum
         , Token.reservedOpNames = ["\\", "->", "MAPCAR", "cons", "cdr", "[", "]", ","]
         }

lexer = Token.makeTokenParser languageDef

var = Token.identifier lexer
parens     = Token.parens lexer
reservedOp = Token.reservedOp lexer
reserved   = Token.reserved lexer

runParser s = parse expr "erro" s
