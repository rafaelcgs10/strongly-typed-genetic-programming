{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
import Control.Monad.State as S
import GHC.Generics (Generic)
import Control.DeepSeq
import System.Posix.Unistd
import Control.Monad.Trans.State.Lazy as L
import Data.Char
import Data.Text as T
import Debug.Trace
import Data.List as Li
import System.Random
import Control.Monad.Except
import Control.Exception as E
import Control.Monad.Trans.Maybe
import qualified Data.Map as Map
import qualified Data.Set as Set

type Id = String
type TVar = String
type TConst = String
data Literal = LitBool !Bool | LitInt !Int | LitChar !Char deriving (Eq, Ord)
data Expr = NamedFunction !Id ![Id] !Expr !Type | Lit !Literal !Type | App !Expr !Expr !Type | Primi Id !Type | Var Id !Type | List ![Expr] !Type | ARG !Type deriving (Eq, Ord,  Generic)
data Type = Dummy !TVar | Generic !TVar | Temporary !TVar | TArrow !Type !Type | TList !Type | TConst !TConst deriving (Eq, Ord)
data EvalError = Fine | NotTerminating | CompError | NotTerminatingCompError deriving (Show)
data Evaluated = Evaluated {expr :: Expr, result :: Expr, er :: EvalError, counts :: Int, fitness :: Float} deriving (Show)

algEvalError Fine Fine = Fine
algEvalError NotTerminating NotTerminating = NotTerminating
algEvalError NotTerminating Fine = NotTerminating
algEvalError Fine NotTerminating = NotTerminating
algEvalError CompError CompError = CompError
algEvalError CompError Fine = CompError
algEvalError Fine CompError = CompError
algEvalError NotTerminating CompError = NotTerminatingCompError
algEvalError CompError NotTerminating = NotTerminatingCompError
algEvalError NotTerminatingCompError _ = NotTerminatingCompError
algEvalError _ NotTerminatingCompError = NotTerminatingCompError
type TSubst = Map.Map TVar Type

aux = Map.fromList [("G1", (TConst "Char")), ("G2", (TConst "Int"))]

data TErro = TUnificationStop Type Type | TInfinity TVar Type | EmptyList | DummyFunction deriving (Show)

type Genetic = ExceptT TErro (S.StateT Count IO)

data Count = Count {temporary_count ::Int} deriving (Show)

data Subs = Subs Id Expr

showMaybe (Just e) = show e
showMaybe Nothing = "no"
instance Show Type where
    show (Dummy a) = a
    show (Generic a) = a
    show (Temporary a) = a
    show (TArrow a b) = show a ++ " -> " ++ show b
    show (TList a) = "[" ++ show a ++ "]"
    show (TConst a) = a

instance Show Expr where
  show e = showExpr e

instance Show Literal where
  show (LitBool b) = show b
  show (LitInt i) = show i
  show (LitChar i) = show i

getTypeExpr (NamedFunction _ _ _ t) = t
getTypeExpr (Lit _ t) = t
getTypeExpr (App _ _ t) = t
getTypeExpr (Primi _ t) = t
getTypeExpr (Var _ t) = t
getTypeExpr (List _ t) = t
getTypeExpr (ARG t) = t

showExpr (App (App (App (Primi f _) e1 _) e2 _) e3 _) | f == "if" = "(" ++ f ++ " " ++ (showExpr e1) ++ " then " ++ (showExpr e2) ++ " else " ++ showExpr e3 ++ ")"
showExpr (App (App (Primi id _) e1 _) e2 _) | id == "+" = "(" ++ showExpr e1 ++ " + " ++  showExpr e2 ++ ")"
                                            | id == "-" = "(" ++ showExpr e1 ++ " - " ++  showExpr e2 ++ ")"
                                            | id == "<" = "(" ++ showExpr e1 ++ " < " ++  showExpr e2 ++ ")"
                                            | id == ">" = "(" ++ showExpr e1 ++ " > " ++  showExpr e2 ++ ")"
                                            | id == "<=" = "(" ++ showExpr e1 ++ " <= " ++  showExpr e2 ++ ")"
                                            | id == ">=" = "(" ++ showExpr e1 ++ " >= " ++  showExpr e2 ++ ")"
                                            | id == "==" = "(" ++ showExpr e1 ++ " == " ++  showExpr e2 ++ ")"
showExpr (App (Primi id _) e t) | id == "cons" = showExpr e ++ " :"
showExpr (App e1 e2 t) = "(" ++ (showExpr e1) ++ " " ++ (showExpr e2) ++ ")"
showExpr (List l t) = (show l)
showExpr (Lit l t) = showLiteral l ++ " " ++ show t
showExpr (Primi id t) = id ++ " " ++ show t
showExpr (Var id t) = id ++ " " ++ show t
showExpr (NamedFunction i args e t)  = "(" ++ i ++ " " ++ (showArgs args) ++ "= " ++ (showExpr e) ++ ")"
showExpr (ARG t) = "ARG" ++ " " ++ show t
showArgs [] = ""
showArgs (x:xs) = (x) ++ " " ++ showArgs xs
showLiteral (LitBool b) = show b
showLiteral (LitChar c) = show c
showLiteral (LitInt i) = show i

--take' = (ARG (TList (Generic "G1")))
fMap = (ARG (TList (Generic "G2")))
nth3 = (ARG (Generic "G1"))

g = [
     (Var "l" (TList (Generic "G1"))),
     --(Var "f" (TArrow (Generic "G1") (Generic "G2"))),
     --(Var "n" (TConst "Int")),
     (List [] (TList (Dummy "a")))
     --(Lit (LitInt 0) (TConst "Int")),
     --(Lit (LitInt 1) (TConst "Int"))
    ]

f = [
     (Primi "if" (TArrow (TConst "Bool") (TArrow (Dummy "a") (TArrow (Dummy "a") (Dummy "a")))) ),
     --(Primi "tail" (TArrow (TList (Dummy "a")) (TList (Dummy "a")))),
     --(Primi "length" (TArrow (TList (Dummy "a")) (TConst "Int"))),
     --(Primi "take" (TArrow (TConst "Int") (TArrow (TList (Generic "G1")) (TList (Generic "G1"))))),
     --(Primi "<=" (TArrow (TConst "Int") (TArrow (TConst "Int") (TConst "Bool")) ) ),
     --(Primi ">" (TArrow (TConst "Int") (TArrow (TConst "Int") (TConst "Bool")) ) ),
     --(Primi "nTH3" (TArrow (TConst "Int") (TArrow (TList (Generic "G1")) (Generic "G1")))),
     --(Primi "-" (TArrow (TConst "Int") (TArrow (TConst "Int") (TConst "Int")) ) ),
     --(Primi "f" (TArrow (Generic "G1") (Generic "G2"))),
     --(Primi "fMap" (TArrow (TArrow (Generic "G1") (Generic "G2")) (TArrow (TList (Generic "G1")) (TList (Generic "G2"))))),
     --(Primi "cons" (TArrow (Dummy "a") (TArrow (TList (Dummy "a")) (TList (Dummy "a"))))),
     (Primi "null" (TArrow (TList (Dummy "a")) (TConst "Bool")))
     --(Primi "head" (TArrow (TList (Dummy "a")) (Dummy "a")))
    ]

checkRestrictions [] e = return $ e
checkRestrictions (r:res) e = do
                            r e
                            result <- checkRestrictions res e
                            return $ result
                        

restrictions :: [Expr -> Genetic Expr]
restrictions = [
               \ e -> case e of
                              (App (App (App (Primi f _) e1 _) e2 _) e3 _) | f == "if" -> if e2 == e3 then throwError DummyFunction else return $ e
                              _ -> return $ e,
               \ e -> case e of 
                              (App (Primi f _) (List [] _) t) | f == "head" -> throwError DummyFunction
                                                              | f == "tail" -> throwError DummyFunction
                              _ -> return $ e
               -- \ e -> case e of
               --              (App (App (Primi f _) (Lit (LitInt n1) _) _) (Var n2 _) t) -> throwError DummyFunction 
               --              _ -> return $ e,
               -- \ e -> case e of
               --                (App (App (Primi f _) (Lit (LitInt n1) _) _) (Lit (LitInt n2) _) t) -> throwError DummyFunction 
               --                _ -> return $ e,
               -- \ e -> case e of
               --                (App (App (Primi f _) e2@(Var n1 _) _) e3@(Var n2 _) t) | f == "-" -> if e2 == e3 then throwError DummyFunction else return $ e
               --                                                                            | f == "<=" -> if e2 == e3 then throwError DummyFunction else return $ e
               --                _ -> return $ e

              ]
     

returnType (TArrow _ t) = returnType t
returnType t = t

compose :: TSubst -> TSubst -> TSubst
s1 `compose` s2 = Map.map (tsubst s1) s2 `Map.union` s1

unify :: Type -> Type -> Genetic TSubst
unify (TArrow l r) (TArrow l' r') = do
                                    s1 <- unify l l'
                                    s2 <- unify (tsubst s1 r) (tsubst s1 r')
                                    return $ s1 `compose` s2

unify (Dummy a) t = bind a t
unify t (Dummy a) = bind a t
unify t1@(Temporary a) t2@(Temporary b) = if a == b then (return Map.empty) else bind a t2
unify t (Temporary a) = bind a t
unify (Temporary a) t = bind a t
unify t1@(Generic g1) t2@(Generic g2) =  if g1 == g2 then (return Map.empty) else throwError $ TUnificationStop t1 t2
unify t1@(TConst c1) t2@(TConst c2) =  if c1 == c2 then (return Map.empty) else throwError $ TUnificationStop t1 t2
unify (TList t1) (TList t2) = unify t1 t2
unify t1 t2 = throwError $ TUnificationStop t1 t2

unifyReturn :: Type -> Type -> Genetic TSubst
unifyReturn t1 t2 = unify (returnType t1) (returnType t2)

bind :: TVar -> Type -> Genetic TSubst
bind a t
        | Dummy a == t = return $ Map.empty
        | occurs a t = throwError $ TInfinity a t
        | otherwise = return $ Map.singleton a t

occurs :: TVar -> Type -> Bool
occurs tv (Dummy t) = if tv == t then True else False
occurs tv (TConst t) = False
occurs tv (Temporary t) = if tv == t then True else False
occurs tv (TList t) = occurs tv t
occurs tv (Generic t) = if tv == t then True else False
occurs tv (TArrow l r) = (occurs tv l) || (occurs tv r)

class Subst a where
    tsubst :: TSubst -> a -> a

instance Subst Type where
    tsubst s (TArrow l r) = TArrow (tsubst s l) (tsubst s r)
    tsubst s (Dummy tv) = case Map.lookup tv s of
                              (Just tv') -> tv'
                              Nothing -> (Dummy tv)
    tsubst s (Generic tv) = case Map.lookup tv s of
                              (Just tv') -> tv'
                              Nothing -> (Generic tv)

    tsubst s (Temporary tv) = case Map.lookup tv s of
                              (Just tv') -> tv'
                              Nothing -> (Temporary tv)
    tsubst s (TList t) = TList (tsubst s t)
    tsubst s t = t

instance Subst Expr where
    tsubst s (App e1 e2 t) = App (tsubst s e1) (tsubst s e2) (tsubst s t)
    tsubst s e@(Lit _ t) = e
    tsubst s (Primi id t) = Primi id (tsubst s t)
    tsubst s (Var id t) = Var id (tsubst s t)
    tsubst s (List e t) = List e (tsubst s t)
    tsubst s (ARG t) = ARG (tsubst s t)


pick :: [a] -> Genetic a
pick l = do
   let len = Li.length l
   if len <= 0 then throwError EmptyList else do
                                              i <- lift $ lift $ randomRIO (0, len - 1)
                                              return $ l !! i

instantiate_temporary (TArrow l r) = do {t1 <- instantiate_temporary l; t2 <- instantiate_temporary (tsubst t1 r); return $ Map.union t1 t2}
instantiate_temporary (Dummy v) = do {v' <- newTemporary; return $ Map.singleton v v'}
instantiate_temporary (TList t) = instantiate_temporary t
instantiate_temporary _ = return $ Map.empty

newTemporary :: Genetic Type
newTemporary = do 
        c <- lift $ L.get
        let newName = "T" ++ [(chr ([49..] !! (temporary_count c)))]
        lift $ L.put (Count {temporary_count  = ((temporary_count c) + 1)})
        return (Temporary newName)

filterUnifyReturn :: Type -> Type -> Genetic Bool
filterUnifyReturn t1 t2 = do {result <- unifyReturn t1 t2; return True} `catchError` (\_ -> return $ False)
filterUnify t1 t2 = do {result <- unify t1 t2; return True} `catchError` (\_ -> return $ False)

sfReturnUnify :: Integer -> Type -> Genetic Expr
sfReturnUnify i t = do
                    let set = if i > 1 then f else g
                    let sf = if i > 1 then filterUnifyReturn else filterUnify
                    candidates <- filterM (\ e -> (sf t (getTypeExpr e))) set
                    selected <- pick candidates
                    let ts = getTypeExpr selected
                    s1 <- unifyReturn t ts
                    traceM $ "s1: " ++ show s1
                    let selectedUnified = tsubst s1 selected
                    s2 <- instantiate_temporary (getTypeExpr selectedUnified)
                    traceM $ "s2: " ++ show s2
                    return $ traceShowId $ tsubst s2 selectedUnified

fillWithARGs :: Integer -> Expr -> Genetic Expr
fillWithARGs i (Primi id t@(TArrow t1 t2)) = do
                                             e <- fillWithARGs (i - 1)  $ App (Primi id t) (ARG t1) t2
                                             return $ e 
fillWithARGs i (App e1 e2 t@(TArrow t1 t2)) = do 
                                             e <- fillWithARGs (i - 1) $ App (App e1 e2 t) (ARG t1) t2
                                             return $ e
fillWithARGs i e | i >= 0 = return $ e
                 | otherwise = throwError DummyFunction
                                    
tVarArg (Temporary v) = v
tVarArg (Dummy v) = v
tVarArg (Generic v) = v
tVarArg (TList t) = tVarArg t

skeleton :: Integer -> Expr -> Genetic (Expr, TSubst)
skeleton _ e@(Primi _ _) = return $ (e, Map.empty) 
skeleton _ e@(Var _ _) = return $ (e, Map.empty) 
skeleton _ e@(Lit _ _) = return $ (e, Map.empty) 
skeleton _ e@(List _ _) = return $ (e, Map.empty) 
--skeleton i@1 e@(ARG t) = trySubSkeleton (i - 1) e
skeleton i e@(ARG t) = do
                          selected <- sfReturnUnify i t
                          traceM $ "selected: " ++ show selected
                          selectedFilled <- fillWithARGs i selected
                          traceM $ "selectedFilled: " ++ show selectedFilled
                          r <-  skeleton i selectedFilled 
                          --checkRestrictions restrictions (fst r)
                          return $ r
                          `catchError` (\ _ -> do { if i == (height - 1) then (skeleton i e) else throwError DummyFunction})
skeleton 0 e = return $ (e, Map.empty)
skeleton i e@(App e1 e2@(ARG t') t) = do
                                      j <- lift $ lift $ randomRIO (1, i - 1)
                                      (argSelected2, sa2) <- skeleton (i - j) e2
                                      traceM $ "sa2: " ++ show sa2
                                      s <- unify t' (getTypeExpr argSelected2)
                                      traceM $ "s: " ++ show s
                                      let te1 = tsubst (s `compose` sa2) e1
                                      j' <- lift $ lift $ randomRIO (1, i - 1)
                                      (argSelected1, sa1) <- skeleton (i - 1) te1
                                      let sa12 = s `compose` (sa1 `compose` sa2)
                                      let applyed = App argSelected1 (argSelected2) (tsubst sa12 t)
                                      return $ (applyed, sa12)
skeleton i e = return $ (e, Map.empty)

trySubSkeleton i e = (skeleton i e) `catchError` (\ e' -> do {(skeleton i e)})
trySkeleton i e = (skeleton i e) `catchError` (\ e' -> do {(trySkeleton i e)})
runSkeleton i e = ((runStateT . runExceptT) (trySkeleton i e) (Count 0))

right (Right e) = e

generateSubject = do 
        (e, s) <- runSkeleton height fMap
        let fre = (fst . right) e
        return $ fre

--geren2 = runEval $ do 
--                   p1' <- rpar $ generateSubject
--                   p2' <- rpar $ generateSubject
--                   p3' <- rpar $ generateSubject
--                   p4' <- rpar $ generateSubject
--                   rseq p1'
--                   return $ (p1', p2', p3', p4')
--ge = do
--     let (p1', p2', p3', p4') = geren2
--     p1 <- p1'
--     p2 <- p2'
--     p3 <- p3'
--     p4 <- p4'
--     return $ (p1, p2, p3, p4)

generatePopulation n = generatePopulation' Set.empty n
generatePopulation' l n | (Set.size l) >= n = return $ Set.toList l
generatePopulation' l n = do
                    --(p1, p2, p3, p4) <- ge
                    --let l' = (Set.insert p1 l)
                    --let l'' = (Set.insert p2 l')
                    --let l''' = (Set.insert p3 l'')
                    p1 <- generateSubject
                    let l' = (Set.insert p1 l)
                    generatePopulation' l' n
                    --if Set.member p l then generatePopulation' l n else generatePopulation' (Set.insert p l) (n - 1)

f_expr = (Primi "c2i" (TArrow (TConst "Char") (TConst "Int")))
l_expr = (List [(Lit (LitChar n) (TConst "Char")) | n <- ['a'..'g']] (TList (TConst "Char")))
args = [f_expr, l_expr]
--vs = [(Lit (LitInt n) (TConst "Int")) | n <- [1..15]]
--l' = (List [(LitInt n) | n <- [1..10]] (TList (TConst "Int")))
putArgs [] e = e
putArgs (a:as) e = putArgs as (App e a t) where
                                              (TArrow _ t) = getTypeExpr e
putHeadNamed s = (NamedFunction "fMap" ["f", "l"] s  (TArrow (TArrow (Generic "G1") (Generic "G2")) (TArrow (TList (Generic "G1")) (TList (Generic "G2")))))

heightSkeleton e = heightSkeleton' e 0
heightSkeleton' (App e1 e2 _) i = if he1 > he2 then he1 else he2 where
                                    he1 = heightSkeleton' e1 (i+1)
                                    he2 = heightSkeleton' e2 (i+1)
heightSkeleton' e i = i

nextRandom i n m = do
                   j <- randomRIO (n, m)
                   if j == i then nextRandom i n m else return $ j

select2 n m = do
              i <- randomRIO (n, m)
              j <- randomRIO (n, m)
              --j <- nextRandom i n m
              return $ (i, j)

randomFloat :: Float -> Float -> IO Float
randomFloat i f = do
                  let max = f * 100000.0
                  n <- randomRIO (i, max)
                  return $ n / 100000.0

findIndexRou _ [] i = -1
findIndexRou n (r:rou) i = if n >= fst r && n < snd r then i else findIndexRou n rou (i+1)
sumRou i [] = []
sumRou i (x:xs) = (i, i + x) : sumRou (i + x) xs

roulette popFitness = do
                      let (pop, fitnesPop) = Li.unzip popFitness
                      rou <- genRoulette fitnesPop
                      let max = (snd . Li.last) rou
                      roulette' popFitness [] rou

roulette' pop newPop rou | (Li.length newPop) == popSize = return $ newPop
roulette' pop newPop rou = do
                           e <- selectRoulette pop rou
                           roulette' pop (e:newPop) rou

genRoulette fitnesPop = do
                        let rou = sumRou 0.0 fitnesPop
                        return $ rou
                         
selectRoulette pop rou = do
                         let max = (snd . Li.last) rou
                         n <- randomFloat 0.0 max
                         let i = findIndexRou n rou 0
                         let e = pop !! i
                         return $ e

tourment pop = tourment' pop []

tourment' pop popNew | (Li.length popNew) == popSize = return $ popNew
tourment' pop popNew = do
                           (i, j) <- select2 0 (popSize - 1)
                           let ei = pop !! i
                           let ej = pop !! j
                           if (snd ei) > (snd ej) then tourment' pop (ei:popNew) else tourment' pop (ej:popNew)

crossPop' pop popNew n | (Set.size popNew) >= n = do {return $ Set.toList popNew}
crossPop' pop popNew n = do
                         (i, j) <- select2 0 (n - 1)
                         let ei = pop !! i
                         let ej = pop !! j
                         let fei = fst ei
                         let fej = fst ej
                         (ei', ej') <- crossOver fei fej
                         let popNew' = Set.insert ei' popNew
                         let popNew'' = Set.insert ej' popNew'
                         crossPop' pop popNew'' n

crossPop pop = do
                 --crossPop' pop (Set.fromList (drop (popSize - 5) ((fst . unzip) pop))) popSize
                 crossPop' pop Set.empty popSize

randomCrossOver pop = do
                      let len = Li.length pop
                      (i, j) <- select2 0 (len - 1)
                      let ei = pop !! i
                      let ej = pop !! j
                      e' <- crossOver ei ej
                      return $ e'

crossAll' pop popNew 0 = return $ popNew
crossAll' pop popNew n = do
                        (e1, e2) <- randomCrossOver pop
                        crossAll' pop (e1:e2:popNew) (n - 2)
crossAll pop = crossAll' pop [] (Li.length pop)

mutation e = do
           mm <- runMutation e
           let fre = (right . fst) mm
           return $ fre
tryMutation e = (mutation' 0 e) `catchError` (\ _ -> do return $ e)
runMutation e = ((runStateT . runExceptT) (tryMutation e) (Count 0))
mutation' :: Integer -> Expr -> Genetic Expr
mutation' n e | n == height = throwError DummyFunction 
mutation' n e@(App e1 e2 t) = do
                         choice1 <- lift $ lift $ randomRIO (1::Int, 3)
                         case choice1 of
                                      1 -> do
                                           choice2 <- lift $ lift $ randomRIO (1::Int, 2)
                                           case choice2 of
                                                        1 -> do
                                                             e1' <- mutation' (n + 1) e1
                                                             if e1' == e1 then throwError DummyFunction else return (App e1' e2 t)
                                                        2 -> do
                                                             e2' <- mutation' (n + 1) e2
                                                             if e2' == e2 then throwError DummyFunction else return (App e1 e2' t)
                                      2 -> do
                                           (e', _) <- skeleton (height - n) (ARG t)
                                           --checkRestrictions restrictions e'
                                           unify (getTypeExpr e') t
                                           if e' == e then throwError DummyFunction else return $ e'
                                      3 -> do 
                                           e' <- mutation2 e
                                           return $ e'
mutation' n e@(Primi _ _) = do
                        return $ e
mutation' n e = do
                        let t = getTypeExpr e
                        (e', _) <- skeleton (height - n) (ARG t)
                        unify (getTypeExpr e') t
                        --checkRestrictions restrictions e'
                        let height' = heightSkeleton e'
                        if e' == e then throwError DummyFunction else return $ e'
mutation2 (App (App (Primi f id) e1 t) e2 t') = do
                                                 let te1 = getTypeExpr e1
                                                 let te2 = getTypeExpr e2
                                                 unify te1 te2
                                                 return $ (App (App (Primi f id) e2 t) e1 t')
mutation2 e = return $ e
                                                
listIndexMutation n = listIndexMutation' n Set.empty []
listIndexMutation' n set l | (Set.size set) == n = return $ l
listIndexMutation' n set l = do
                             i <- randomRIO (0::Int, n - 1)
                             if Set.member i set then listIndexMutation' n set l else listIndexMutation' n (Set.insert i set) (i:l)

mutationPop pop = do 
                  l <- listIndexMutation (div popSize 100)
                  mutationPop' pop (Set.fromList pop) l
mutationPop' pop popSet [] = return $ Set.toList popSet
mutationPop' pop popSet (i:is) = do
                                 let e = pop !! i
                                 m1 <- mutation e
                                 m2 <- mutation e
                                 m3 <- mutation e
                                 let set = Set.delete e popSet
                                 let m = if m1 /= e then m1 else if m2 /= e then m2 else if m3 /= e then m3 else e
                                 let set' = if (Set.member m set) then popSet else Set.insert m set
                                 mutationPop' (Set.toList set') set' is

crossOver e e' = do
           mm <- runCrossOver e e'
           let fre = (right . fst) mm
           return $ fre

tryCrossOver e e' = (crossOver' 1 e e') `catchError` (\ a -> do return $ (e, e'))
runCrossOver e e' = ((runStateT . runExceptT) (tryCrossOver e e') (Count 0))
crossOver'' n e' e@(App e1 e2 t) = do
                                 s <- unify (t) ((getTypeExpr e'))
                                 let heighte = heightSkeleton e
                                 return $ (tsubst s e, tsubst s e', s)
                                 --if e' /= e then return $ (e, e') else throwError DummyFunction
                                 `catchError` (\_ -> do 
                                                     (e1', e'', s) <- crossOver'' n e' e1
                                                     return $ (e1', (App e'' e2 t), s)
                                                     `catchError` (\_ -> do
                                                                         (e2', e''', s) <- crossOver'' n e' e2
                                                                         return $ (e2', (App e1 e''' t), s)
                                                                  )
                                              )
                                              
crossOver'' n e' e = do
                   s <- unify (getTypeExpr e) (getTypeExpr e')
                   let heighte = heightSkeleton e
                   if (n > heighte) && (getTypeExpr e) == (getTypeExpr e') then return $ (e, e', s) else throwError DummyFunction

crossOver' :: Integer -> Expr -> Expr -> Genetic (Expr, Expr)
crossOver' n e@(App e1 e2 t) e' = do
                                   let choice' = if n > 0 then randomRIO (1::Int, 3) else randomRIO (1::Int, 2)
                                   choice <- lift $ lift $ choice'
                                   case choice of
                                      1 -> do
                                           (e1', e'') <- crossOver' (n + 1) e1 e'
                                           let heighte'' = heightSkeleton e''
                                           let heighte1' = heightSkeleton e1'
                                           if heighte'' > (height) || heighte1' > (height - n) then throwError DummyFunction else return $ ((App e1' e2 t), e'')
                                      2 -> do
                                           (e2', e'') <- crossOver' (n + 1) e2 e'
                                           let heighte'' = heightSkeleton e''
                                           let heighte2' = heightSkeleton e2'
                                           if heighte'' > (height)  || heighte2' > (height - n) then throwError DummyFunction else return $ ((App e1 e2' t), e'')
                                      3 -> do
                                           (e''', e'', s) <- crossOver'' (height - n) e e'
                                           --checkRestrictions restrictions e''
                                           --checkRestrictions restrictions e'''
                                           return $ (tsubst s e''', tsubst s e'')
                                           
crossOver' n e e' = do
                  (e''', e'', s) <- crossOver'' (height - n) e e'
                  --checkRestrictions restrictions e''
                  --checkRestrictions restrictions e'''
                  return $ (tsubst s e''', tsubst s e'')

mapC i f l = if Li.length l <= 0 then [] else (f (Li.head l), i + 1) : mapC (i+1) f (Li.tail l)

evalPopulation count pop = do
                             let namePop = Li.zip (repeat "fMap") (Li.map (tsubst aux) pop)
                             let withHead = Li.map putHeadNamed (Li.map (tsubst aux) pop)
                             let withArgs = Li.map (putArgs args) withHead 
                             --mapM_ ((putStrLn) . (\e -> show e)) withArgs
                             let namePopWithArgs = Li.zip namePop withArgs
                             let resultsEr = Li.map (\(np, wa) -> eval' maxEval np wa) namePopWithArgs
                             let fitnessCount = mapC count (fitnessFMAP (args !! 1)) resultsEr
                             let (fitnes, c) = Li.unzip fitnessCount
                             let (results, ers) = Li.unzip resultsEr
                             let evaluateds = Li.zipWith5 Evaluated pop results ers c fitnes
                             let sortedEvaluateds = sortBy (\x y -> compare (fitness x) (fitness y)) evaluateds
                             return $ sortedEvaluateds

evolution opt l count pop n =  do 
                                sortedEvaluateds <- evalPopulation count pop 
                                let med = (sum (Li.map fitness sortedEvaluateds)) / (fromIntegral popSize)
                                let best = Li.last sortedEvaluateds
                                let bestF = fitness best
                                let bestC = (counts best) :: Int
                                let fopt = if opt /= Nothing then opt else if bestF == 1.0 then Just bestC else opt
                                --traceM $ show n ++ " " ++ show fopt ++ " " ++ show bestF ++ " " ++ show med
                                --putStrLn $  show med  ++ " " ++ show (fitness (last sortedEvaluateds)) ++ " " ++ show (fitness (head sortedEvaluateds))
                                --if (fitness . last) sortedEvaluateds == 1.0 || n <= 0 then do 
                                if n <= 0 then do 
                                          return $ (med:l, fopt)
                                          else do
                                          let popFitness = Li.zip (Li.map expr sortedEvaluateds) (Li.map fitness sortedEvaluateds)
                                          popInter <- tourment popFitness
                                          popCrossed <- crossPop popInter
                                          popMutaded <- mutationPop popCrossed
                                          r <- evolution fopt (med:l) (count + popSize) popMutaded (n - 1)
                                          return $ r
                  
--seed = 132188118366621323
--seed = 213021390219789423
seed = 102938192389000000
--seed = 1166661116666111
height = 6
popSize = 1::Int
maxEval = 9
maxGen = 200::Int
maxFitness = 170.0
testsN = 2

mediaDesvio :: [Float] -> (Float, Float)
mediaDesvio l = (m, d)  where
                      lenl = Li.length l
                      m = ((/(fromIntegral lenl)) . sum) l
                      d = sqrt $ (sum [(e - m) ** 2 |e <- l]) / ((fromIntegral lenl) - 1.0)

unMaybe [] = []
unMaybe ((Just e):xs) = e: unMaybe xs
unMaybe ((Nothing):xs) = unMaybe xs

runTests' res n optl = do
                                             traceM $ show n
                                             pop <- generatePopulation popSize
                                             mapM_ ((putStrLn) . (\e -> show e)) pop
                                             undefined
                                             --(r, fopt) <- evolution Nothing [] 0 pop maxGen 
                                             ----traceM $ show r
                                             --let fopt' = deepseq fopt fopt 
                                             --let rr = deepseq r' r' where
                                             --                       r' = Li.reverse $! r
                                             --runn <- runTests' (rr:res) (n-1) (fopt':optl)
                                             --let runn' = deepseq runn runn 
                                             --return $! runn'
runTests n = do
              runTests' [] n []

main = do
    --setStdGen $ mkStdGen seed
    gen <- getStdGen
    (fits, mopts, opts, lastRes) <- runTests testsN
    --let file_fits = "fits_1000.dat"
    --let file_mopts = "mopts_1000.dat"
    --let file_opts = "opts_1000.dat"
    --let file_last = "last_1000.dat"
    --writeFile file_fits ""
    --writeFile file_opts ""
    --writeFile file_mopts ""
    --writeFile file_last ""
    --mapM_ ((appendFile file_fits) . (\(i, (e1, e2)) -> show i ++ " " ++ show e1 ++ " " ++ show e2 ++ "\n")) $ Li.zip [1..] fits
    --mapM_ ((appendFile file_mopts) . (\(e1, e2) -> show e1 ++ " " ++ show e2 ++ "\n")) [mopts]
    --mapM_ ((appendFile file_opts) . (\e -> showMaybe e ++ "\n")) opts
    --mapM_ ((appendFile file_last) . (\e -> show e ++ "\n")) lastRes
    return ()
--------------------------
--Evaluation

varSubst :: Subs -> Expr -> Expr
varSubst (Subs id e) a@(Var id' t) = if id == id' then e else a
varSubst (Subs id e) a@(Primi id' t) = if id == id' then e else a
varSubst s (App e e' t) = App (varSubst s e) (varSubst s e') t
varSubst s e = e

bReduction i f' e@(App (App (App (Primi f _) e1 _) e2 _) e3 _) | f == "if" = case eval (i) f' $! e1 of 
                                                                                     ((Lit (LitBool True) _), flag) -> (e2, flag)
                                                                                     ((Lit (LitBool False) _), flag) -> (e3, flag)

bReduction _ _ (App (App (Primi f _) (Lit (LitInt n1) _) _) (Lit (LitInt n2) _) t)
                                                                               | f == "+"  = (Lit (LitInt (n1 + n2)) t, Fine)
                                                                               | f == "-"  = (Lit (LitInt (n1 - n2)) t, Fine)
                                                                               | f == "<"  = (Lit (LitBool (n1 < n2)) t, Fine)
                                                                               | f == ">"  = (Lit (LitBool (n1 > n2)) t, Fine)
                                                                               | f == ">=" = (Lit (LitBool (n1 >= n2)) t, Fine)
                                                                               | f == "<=" = (Lit (LitBool (n1 <= n2)) t, Fine)
                                                                               | f == "==" = (Lit (LitBool (n1 == n2)) t, Fine)

bReduction _ _ e@(App (Primi f _) (List l@[] _) t)
                                             | f == "head" = case t of
                                                                    (TConst "Int")  -> (Lit (LitInt 100) t, CompError)
                                                                    (TConst "Char") -> (Lit (LitChar 'z') t, CompError)
                                                                    (TConst "Bool") -> (Lit (LitBool False) t, CompError)
                                                                    (TList _)       -> (List [] t, CompError)

                                             | f == "tail" = case t of
                                                                     (TConst "Int")  -> (Lit (LitInt 100) t, CompError)
                                                                     (TConst "Char") -> (Lit (LitChar 'z') t, CompError)
                                                                     (TConst "Bool") -> (Lit (LitBool False) t, CompError)
                                                                     (TList _)       -> (List [] t, CompError)
                                             | f == "length" = (Lit (LitInt 0) t, Fine)
                                             | f == "null" = (Lit (LitBool True) t, Fine)
bReduction _ _ (App (Primi f _) (Lit (LitChar i) _) t) |
                                                      f == "c2i" = (Lit (LitInt ((\x -> ord x - 97) i)) (TConst "Int"), Fine)
bReduction _ _ (App (Primi f _) (List l@(x:xs) _) t)
                                               | f == "head" = (x, Fine)
                                               | f == "tail" = (List xs t, Fine)
                                               | f == "length" = (Lit (LitInt (fromIntegral (Li.length l))) t, Fine)
                                               | f == "null" = (Lit (LitBool False) t, Fine)
bReduction _ _ e@(App (App (Primi f _) n1 _) (List (xs) _) t)
                                                          | f == "cons" = (List (n1:xs) t, Fine)
bReduction i f (NamedFunction _ [] e _) = eval (i) f e
bReduction i' f (App (NamedFunction i [] e t) e' t') = ((App e e' t'), Fine)
bReduction i' f e''@(App (NamedFunction i (x:xs) e t) e' t') = bReduction i' f $ (NamedFunction i xs (varSubst (Subs x e') e) t')
bReduction _ _ e =  (e, Fine)

eval' i f e = eval i f e
eval :: Int -> (String, Expr) -> Expr -> (Expr, EvalError)
eval i f e | i <= 0 && returnType (getTypeExpr e) == (TConst "Int") = ((Lit (LitInt 100) (TConst "Int")), NotTerminating)
           | i <= 0 && returnType (getTypeExpr e) == (TConst "Bool") = ((Lit (LitBool False) (TConst "Bool")), NotTerminating)
           | i <= 0 && returnType (getTypeExpr e) == (TConst "Char") = ((Lit (LitChar 'z') (TConst "Char")), NotTerminating)
           | i <= 0 && returnType (getTypeExpr e) == (TList (TConst "Int")) = (List [(Lit (LitInt 100) (TConst "Int"))] (TList (TConst "Int")), NotTerminating)
           | i <= 0 && returnType (getTypeExpr e) == (TList (TConst "Char")) = (List [(Lit (LitChar 'z') (TConst "Char"))] (TList (TConst "Char")), NotTerminating)

eval i f@(name, function) e@(App (App (Primi f' tf) e1 t1) e2 t2) | f' == name = (re2, algEvalError fe1 fe2) where
                                                                                                             (re2, fe2) = eval (i-1) f re1 
                                                                                                             (re1, fe1) = bReduction i f $ App (App (putHeadNamed function) e1 t1) e2 t2

eval i f (NamedFunction _ [] e _)  = eval (i) f e
eval i f e@(App (NamedFunction _ _ _ _) _ _) = bReduction (i) f e

eval i f e@(App (App (App (Primi f' t) e1 t1) e2 t2) e3 t3) | f' == "if"  =  (re2, algEvalError fe1 fe2) where 
                                                                                                         (re2, fe2) = eval (i) f re1
                                                                                                         (re1, fe1) = bReduction (i) f e

eval i f e@(App (App (Primi f' tf) e1 t1) e2 t2) = (re4, Li.foldl algEvalError Fine [fe4, fe3, fe2, fe1]) where 
                                                                                                       (re4, fe4) = eval (i) f re3
                                                                                                       (re3, fe3) = bReduction (i) f (App (App (Primi f' tf) re1 t1) re2 t2)
                                                                                                       (re1, fe1) = eval i f e1
                                                                                                       (re2, fe2) = eval i f e2

eval i f e@(App m n t) = (re3, Li.foldl algEvalError Fine [fe3, fe2, fe1]) where
                                                                        (re3, fe3) = bReduction i f (App re1 re2 t)
                                                                        (re1, fe1) = eval i f m
                                                                        (re2, fe2) = eval i f n
eval i f e = (e, Fine)

distance n1 n2 l = abs (p1 - p2) where 
                                 p1 = case elemIndex n1 l of
                                                          (Just p) -> p
                                                          Nothing ->  (Li.length l) - 1
                                 p2 = case elemIndex n2 l of
                                                          (Just p) -> p
sL d rtError reError = 10 * 2**(-d) - (10 * rtError) - (10 * reError)

fitnessNTH3 n1 (List l _) n2 = sL (fromIntegral d) 0 0 where
                                                             d = distance n1 n2 l
fitnessNTH3 _ _ e =  666.0

fitnessNTH3B n1 (List l _) n2 = if n2 == n1 then 1.0 else 0

distance2 e@(Lit (LitInt e') _) l  | elem e l = fromIntegral $ abs (pe - e') where
                                                        (Just pe) = elemIndex e l
distance2 _ _ = 10000.0::Float

checkErrorEval CompError l = -(10.0 + 2.0 * fromIntegral (Li.length l))
checkErrorEval NotTerminating l = -(10.0 + 2.0 * fromIntegral (Li.length l))
checkErrorEval NotTerminatingCompError l = -2.0*(10.0 + 2.0 * fromIntegral (Li.length l))
checkErrorEval Fine l = 0.0

fitnessFMAP (List l _) ((List l' _), er) = (100.0 - 2.0 * (fromIntegral ((abs ( (Li.length l) - (Li.length l') )))) + ( ((sum ((Li.map (\e -> 10.0 * (2 ** (-1.0 * distance2 e l'))) (nub l')))))) + checkErrorEval er l) / maxFitness
